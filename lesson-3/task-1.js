/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''
    // РЕШЕНИЕ НАЧАЛО
    const result = (typeof str !== 'string') ? str = 'Please enter the string!' : str.charAt(0).toUpperCase() + str.slice(1);
    // РЕШЕНИЕ КОНЕЦ

    return result;
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''