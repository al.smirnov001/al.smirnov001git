import { saveToStorage } from './login';

console.log('wizard');

const user = {};
const registerButton = document.getElementById('regBtn');
const continueButton = document.getElementById('toStep2Btn');
const from1to2Button = document.getElementById('from1to2');
const from2to1Button = document.getElementById('from2to1');
const toLoginSvgButton = document.getElementById('toLoginSvg');
const from3to2SvgButton = document.getElementById('from3to2Svg');
const createAccountButton = document.getElementById('createAccount');
const loginBlock = document.getElementById('loginBlock');
const step1Block = document.getElementById('step1Block');
const regBlock = document.getElementById('regBlock');

const loginFormShow = function () {
    loginBlock.style.display = "flex";
	regBlock.style.display = "none";
    step1Block.style.display = "none";
}

const step1BlockShow = function () {
	step1Block.style.display = "flex";
    loginBlock.style.display = "none";
    regBlock.style.display = "none";
}

const regBlockShow = function () {
	regBlock.style.display = "flex";
    step1Block.style.display = "none";
    loginBlock.style.display = "none";
}

registerButton.addEventListener('click', (event) => {
    step1BlockShow();
});

from2to1Button.addEventListener('click', (event) => {
    step1BlockShow();
});

continueButton.addEventListener('click', (event) => {
	let userType = document.querySelector('.user-wrapper input:checked').id.replace('user_','');
    user.type = userType;
    if (user.type === 'teacher') {
    	if(!localStorage.getItem('teacher')) {
    		regBlockShow();
	    } else {
	    	alert('You can not register as a teacher! One teacher has been already registered.');	
	    }
    } else {
    	regBlockShow();
    }
});

from1to2Button.addEventListener('click', (event) => {
    regBlockShow();
});

toLoginSvgButton.addEventListener('click', (event) => {
    loginFormShow();
});

from3to2SvgButton.addEventListener('click', (event) => {
    step1BlockShow();
});

createAccountButton.addEventListener('click', (event) => {
	let userName = document.getElementById('name');
	let userEmail = document.getElementById('email');
	let userPassword = document.getElementById('password');
	let userPasswordNext = document.getElementById('password_next');
	
	const passwordIsValid = function () {
		return userPassword.value === userPasswordNext.value;
	}

	const nameIsValid = function () {
		return userName.value.split(' ').length === 2 && userName.value.split(' ')[0].length > 2 && userName.value.split(' ')[1].length > 2;
	}

	user.name = userName.value;
	user.email = userEmail.value;
	user.password = userPassword.value;

	if (user.type === 'teacher') {
	    if (!passwordIsValid()) {
			userPassword.classList.add("error");
			userPasswordNext.classList.add("error");
		} else {
			userPassword.classList.remove("error");
			userPasswordNext.classList.remove("error");
		}

		if (!nameIsValid()) {
			userName.classList.add("error");
		} else {
			userName.classList.remove("error");			
		}

		if (nameIsValid() && passwordIsValid()) {
			localStorage.setItem('teacher', JSON.stringify(user));
			saveToStorage(user);
			window.location.href = user.type + '.html';		    
		}
	}

	if (user.type === 'student') {
		let allStudents;
	    if (!localStorage.getItem('students')) {
	    	allStudents = [];
	    	allStudents.push(user);
	    } else {
	    	allStudents = JSON.parse(localStorage.getItem('students'));
	    	allStudents.push(user);
	    }	    
	    if (!passwordIsValid()) {
			userPassword.classList.add("error");
			userPasswordNext.classList.add("error");
		} else {
			userPassword.classList.remove("error");
			userPasswordNext.classList.remove("error");
		}

		if (!nameIsValid()) {
			userName.classList.add("error");
		} else {
			userName.classList.remove("error");			
		}

		if (nameIsValid() && passwordIsValid()) {
			localStorage.setItem('students', JSON.stringify(allStudents));
			saveToStorage(user);
			window.location.href = user.type + '.html';		    
		}
	}
});
