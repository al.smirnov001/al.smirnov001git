import { timeSlots } from './constants';
import { lessons } from './constants';

console.log('lessons form');

const bookLessonForm = document.getElementById('formLessons');
const isTomorrow = function (time) {
	return time.includes('1') || time.includes('2') || time.includes('3');
}

bookLessonForm.addEventListener('submit', (event) => {
	const timeChecked = document.querySelector('input[type="radio"][name="time"]:checked').id;
	const lessonChecked = document.querySelector('input[type="radio"][name="type"]:checked').id;
	const studentName = JSON.parse(localStorage.getItem('login')).name;
	const lessonTime = timeSlots[timeChecked];
	const lessonTitle = lessons[lessonChecked].title;
	const lessonDuration = lessons[lessonChecked].duration;

	const allLessons = JSON.parse(localStorage.getItem('lessons')) || [];
	const lesson = {};

	lesson.name = studentName;
	lesson.time = lessonTime;
	lesson.tomorrow = isTomorrow(timeChecked);
	lesson.title = lessonTitle;
	lesson.duration = lessonDuration;

	allLessons.push(lesson);
	localStorage.setItem('lessons', JSON.stringify(allLessons));
});