console.log('planed lessons');

const savedLessonsBlock = document.getElementById('planedLessons');

const savedLessons = JSON.parse(localStorage.getItem('lessons')) || [];

const getLesson = (lesson) => {
    const { name, time, tomorrow, title, duration } = lesson;
    const day = lesson.tomorrow === true ? 'Завтра' : 'Сегодня';
    const schedule = function () {
    	const startTime = time + '.00';
	    const endTime = lesson.duration === 60 ? time + 1 + '.00' : time + 1 + '.30';
	    return startTime + ' — ' + endTime;
    }

    return `
        <div class="card-box">
			<div class="card-illustration">
				<img src="./images/user_03.png" alt="">
			</div>
			<div class="info">
				<p class="sub-title">${day}, ${schedule()}</p>
				<p class="info-title">${lesson.name}</p>
				<p class="info-desc">${lesson.title}</p>
			</div>
		</div>`;
};

const generateHTML = () => {
    const lessonsHTML = savedLessons.map((lesson) => {
        return getLesson(lesson);
    }).join('');
    
    savedLessonsBlock.insertAdjacentHTML('afterend', lessonsHTML);
};

document.querySelectorAll('.block__scheduled-lessons .card-box').forEach(function(item){
    item.remove();
});

generateHTML();