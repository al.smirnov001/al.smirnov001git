/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ
function addForm () {
	const body = document.querySelector('body');
	const form = document.createElement('form');
	const formGroup = document.createElement('div');
	const label = document.createElement('label');
	const input = document.createElement('input');
	const button = document.createElement('button');
	const setAttributes = function (el, attrs) {
	  for(var key in attrs) {
	    el.setAttribute(key, attrs[key]);
	  }
	}

	form.setAttribute('id', 'form');
	formGroup.className = 'form-group';
	input.className = 'form-control';
	button.className = 'btn btn-primary';
	button.setAttribute('type', 'submit');
	button.innerHTML = 'Вход';

	formGroup.innerHTML += label.outerHTML + input.outerHTML;
	form.innerHTML += formGroup.outerHTML + formGroup.outerHTML + formGroup.outerHTML + button.outerHTML;
	body.append(form);

	document.querySelectorAll(".form-group")[2].classList.add('form-check');

	document.querySelectorAll('label')[0].setAttribute('for', 'email');
	document.querySelectorAll('label')[0].innerHTML = 'Электропочта';
	document.querySelectorAll('label')[1].setAttribute('for', 'password');
	document.querySelectorAll('label')[1].innerHTML = 'Пароль';
	document.querySelectorAll('label')[2].setAttribute('for', 'exampleCheck1');
	document.querySelectorAll('label')[2].className = 'form-check-label';
	document.querySelectorAll('label')[2].innerHTML = 'Запомнить меня';

	setAttributes(document.querySelectorAll('input')[0], {'type': 'email', 'id': 'email', 'placeholder': 'Введите свою электропочту'});
	setAttributes(document.querySelectorAll('input')[1], {'type': 'password', 'id': 'password', 'placeholder': 'Введите пароль'});
	setAttributes(document.querySelectorAll('input')[2], {'type': 'checkbox', 'id': 'exampleCheck1'});
	document.querySelectorAll('input')[2].className = 'form-check-input';
}

addForm();

(() => {
  const list = document.querySelector(".form-check");
  list.appendChild(list.firstElementChild);
})();