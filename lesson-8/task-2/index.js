/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ
function addForm () {
	const body = document.querySelector('body');
	const form = document.getElementById('form');
	const formGroup = document.createElement('div');
	const label = document.createElement('label');
	const input = document.createElement('input');
	const button = document.createElement('button');
	const setAttributes = function (el, attrs) {
	  for(var key in attrs) {
	    el.setAttribute(key, attrs[key]);
	  }
	}

	formGroup.className = 'form-group';
	input.className = 'form-control';
	button.className = 'btn btn-primary';
	button.setAttribute('type', 'submit');
	button.innerHTML = 'Вход';

	formGroup.innerHTML += label.outerHTML + input.outerHTML;
	form.innerHTML += formGroup.outerHTML + formGroup.outerHTML + formGroup.outerHTML + button.outerHTML;
	body.append(form);

	document.querySelectorAll(".form-group")[2].classList.add('form-check');

	document.querySelectorAll('label')[0].setAttribute('for', 'email');
	document.querySelectorAll('label')[0].innerHTML = 'Электропочта';
	document.querySelectorAll('label')[1].setAttribute('for', 'password');
	document.querySelectorAll('label')[1].innerHTML = 'Пароль';
	document.querySelectorAll('label')[2].setAttribute('for', 'exampleCheck1');
	document.querySelectorAll('label')[2].className = 'form-check-label';
	document.querySelectorAll('label')[2].innerHTML = 'Запомнить меня';

	setAttributes(document.querySelectorAll('input')[0], {'type': 'email', 'id': 'email', 'placeholder': 'Введите свою электропочту'});
	setAttributes(document.querySelectorAll('input')[1], {'type': 'password', 'id': 'password', 'placeholder': 'Введите пароль'});
	setAttributes(document.querySelectorAll('input')[2], {'type': 'checkbox', 'id': 'exampleCheck1'});
	document.querySelectorAll('input')[2].className = 'form-check-input';
}

addForm();

(() => {
  const list = document.querySelector(".form-check");
  list.appendChild(list.firstElementChild);
})();

document.querySelector('[type="submit"]').onclick = (event) => {
    event.preventDefault();

	let obj;
	let email = document.getElementById('email').value;
	let password = document.getElementById('password').value;
	let remember = document.getElementById('exampleCheck1').checked;

	if (!email || !password) {
		throw new Error('Empty values!')
	}

    obj = {email: email, password: password, remember: String(remember)}
    console.log(obj);
}