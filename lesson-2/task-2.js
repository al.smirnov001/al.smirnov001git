// Перепишите `if..else` с использованием нескольких операторов `?`. 
// Для читаемости — оформляйте код в несколько строк.

////////////////// Задание //////////////////
// let message;

// if (login === 'Pitter') {
//   message = 'Hi';
// } else if (login === 'Owner') {
//   message = 'Hello';
// } else if (login === '') {
//   message = 'unknown';
// } else {
//   message = '';
// }

////////////////// Решение //////////////////

var message = (login === 'Pitter') ? message = 'Hi' :
	(login === 'Owner') ? message = 'Hello' :
	(login === '') ? message = 'unknown' :
	message = '';