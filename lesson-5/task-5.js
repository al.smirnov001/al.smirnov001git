/**
 * Задача 5.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ

const createFibonacciGenerator = function () {
	let num1 = 0;
	let num2 = 0;
	
	const generator = {
		print: function () {
			if (num2 === 0) {
				num2 = 1;
				return num2;
			}

	        var nextNum = num1 + num2;

	        num1 = num2;
	        num2 = nextNum;

	        return nextNum;
	    },
		reset: function () {
	        num1 = 0;
	        num2 = 0;
	    }	    
	}

	return generator;	
}

const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

exports.createFibonacciGenerator = createFibonacciGenerator;