/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ
const f = function () {
	let sum = 0;
	for (i in arguments) {
		let argument = arguments[i];
		if (typeof argument !== "number") {
		    throw new Error(`${argument} is not a number!`);
		}
		sum += argument;
	}
	return sum;
} 

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9