/**
 * Напишите функцию для форматирования даты.
 * 
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 * 
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
*/

const formatDate = (date, format, delimiter) => {
    // РЕШЕНИЕ
    let result = '';
    const getDate = function (arg) {
    	let dd = arg.getDate();
    	if (dd < 10) {
    		dd = '0' + dd;
    	}
    	return dd.toString();
    }
    const getMonth = function (arg) {
    	let mm = arg.getMonth();
    	if (mm < 10) {
    		mm = '0' + mm;
    	}
    	return mm.toString();
    }
    const DD = getDate(date);
    const MM = getMonth(date);
    const YYYY = date.getFullYear().toString();

    const formatIsValid = function (format) {
    	return format === 'DD MM YYYY' || format === 'MM DD YYYY' || format === 'YYYY MM DD' || format === 'YYYY DD MM' || format === 'MM YYYY' || format === 'YYYY MM' || format === 'YYYY' || format === 'DD MM' || format === 'MM DD';
    }
    const delimiterIsValid = function (delimiter) {
    	return delimiter === '-' || delimiter === '.' || delimiter === '/' || !delimiter
    }

    if (!formatIsValid(format)) {
    	throw new Error(`Format of the date is not valid!`);
    }

    if (!delimiterIsValid(delimiter)) {
    	throw new Error(`Delimiter is not valid!`);
    }

    if (!delimiter) {
    	delimiter = '.';    	
    }

    for (const i of format.split(' ')) {
        if (i === 'DD') {
        	result += DD+delimiter;
        } else if (i === 'MM') {
			result += MM+delimiter;
        } else if (i === 'YYYY') {
        	result += YYYY+delimiter;
        }
    }
    result = result.substring(0, result.length - 1);

    return result;
};

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
