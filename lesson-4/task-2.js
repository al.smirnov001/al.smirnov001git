/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение
function reduce(array, func, accumulator) {
    var res = null;

    if (!Array.isArray(array)) {
        throw new Error('Please use array as the first parameter!')
    }

    if (typeof func !== "function") {
        throw new Error('Please use function as the second parameter!')
    }

    if (!accumulator) {
    	accumulator = 0;
    }

    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        res = func(accumulator, element);
        accumulator = res;
    }

    return res;
}

function getSum (accumulator, element) {
    return accumulator + element;
}

const result = reduce(array, getSum, INITIAL_ACCUMULATOR);

console.log(result); // 21