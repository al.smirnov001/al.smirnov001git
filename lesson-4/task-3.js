/**
 * Задача 3.
 *
 * Напишите функцию `inspect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — новый массив.
 * Этот новый массив должен содержать исключительно длины строк, которые были в
 * переданном массиве.
 * Если строк в переданном массиве не было — нужно вернуть пустой массив.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива filter;
 * - Обязательно использовать встроенный метод массива map.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив.
 */

const array = [
    false,
    'Привет.',
    2,
    'Здравствуй.',
    [],
    'Прощай.',
    {
        name: 'Уолтер',
        surname: 'Уайт',
    },
    'Приветствую.',
];

// Решение

function inspect(arr) {
    const newArr = [];
    const resultArr = [];

    if (!Array.isArray(arr)) {
        throw new Error('Please use array as the first parameter!')
    }

    arr.filter(function(element){
        if (typeof element === 'string') {
            newArr.push(element);
        }
    });
    
    newArr.map(function(element){
        resultArr.push(element.length);
    });

    return resultArr;
}

const result = inspect(array);
console.log(result); // [ 7, 11, 7, 12 ]