/**
 * Задача 1.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

// Решение
function filter(array, func) {
    const newArr = [];

    if (!Array.isArray(array)) {
        throw new Error('Please use array as the first parameter!')
    }

    if (typeof func !== "function") {
        throw new Error('Please use function as the second parameter!')
    }

    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        const isValid = func(element);

        if (isValid) {
            newArr.push(element);
        }
    }

    return newArr;
}

function findGoodEvening(element) {
    return element === 'Добрый вечер!';
}

const filteredArray = filter(array, findGoodEvening);

console.log(filteredArray); // ['Добрый вечер!']